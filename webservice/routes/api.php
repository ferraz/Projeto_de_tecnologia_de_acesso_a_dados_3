<?php

use Illuminate\Http\Request;

use App\User;

use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/cadastro', function (Request $request) {
    
    //Obtendo os dados atráves de requisição.
    $data = $request->all();
    
    $validacao = Validator::make($data, [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => 'required|string|min:6|confirmed',
    ]);

    if($validacao->fails()){
        return $validacao->errors();
    }

    $user = User::create([
        'name' => $data['name'],
        'email' => $data['email'],
        'password' => bcrypt($data['password']),
    ]);

    $user->token = $user->createToken($user->email)->accessToken;
    
    return $user;
    
});

Route::post('/login', function (Request $request) {

    /*
    O método all vai entregar um array para data
    $data = $request->all();
    */

    $validacao = Validator::make($request->all(), [
        'email' => 'required|string|email|max:255',
        'password' => 'required|string',
    ]);

    if ($validacao->fails()) {
        return $validacao->errors();
        
    }
    if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

        //Obtendo acesso aos dados do usuário logado.
        $user = Auth()->user();
        $user->token = $user->createToken($user->email)->accessToken;
        return $user;

    } else {

        return false;
    }
    
    
});

Route::middleware('auth:api')->get('/usuarios', function (Request $request) {
    return User::all();
});


